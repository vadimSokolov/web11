$(document).ready(function () {

  

  if (window.matchMedia('(max-width: 768px)').matches) {
    $('.slider').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      dots: true,
    });
  } else {
    $('.slider').slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 4,
      dots: true,
    });
  }

});

